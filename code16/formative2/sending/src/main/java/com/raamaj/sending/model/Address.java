package com.raamaj.sending.model;

public class Address {
	private String recipient;
	private String gender;
	private String street;
	private String zipCode;
	private String district;
	private String city;
	private String province;
	private String country;
	private boolean assurance;
	private String description;
	private int totalPrice;
	private int deliveryCost;
	public Address() {}
	public Address(String recipient, String gender, String street, String zipCode, String district, String city,
			String province, String country, boolean assurance, String description, int totalPrice, int deliveryCost) {
		super();
		this.recipient = recipient;
		this.gender = gender;
		this.street = street;
		this.zipCode = zipCode;
		this.district = district;
		this.city = city;
		this.province = province;
		this.country = country;
		this.assurance = assurance;
		this.description = description;
		this.totalPrice = totalPrice;
		this.deliveryCost = deliveryCost;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public boolean isAssurance() {
		return assurance;
	}
	public void setAssurance(boolean assurance) {
		this.assurance = assurance;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}
	public int getDeliveryCost() {
		return deliveryCost;
	}
	public void setDeliveryCost(int deliveryCost) {
		this.deliveryCost = deliveryCost;
	}
}
