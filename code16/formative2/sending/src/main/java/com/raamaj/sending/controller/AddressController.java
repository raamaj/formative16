package com.raamaj.sending.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.raamaj.sending.model.Address;
import com.raamaj.sending.model.AddressDao;

@Controller
public class AddressController {
	@Autowired
	AddressDao addressDao; 
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public @ResponseBody RedirectView save(Address address) {
		addressDao.save(address);
		return new RedirectView("/sending");
	}
}
