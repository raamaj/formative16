package com.raamaj.sending.model;

import org.springframework.jdbc.core.JdbcTemplate;

public class AddressDao {
	JdbcTemplate template;
	
	public void setTemplate(JdbcTemplate template) {    
	    this.template = template;    
	} 
	
	public int save(Address address) {
		String sql = "insert into address "
				+ "(recipient,gender,street,zipCode,district,city,province,"
				+ "country,assurance,description,totalPrice,deliveryCost) values("
				+ "'" + address.getRecipient() + "','" + address.getGender() + "','" + address.getStreet()
				+ "','" + address.getZipCode() + "','" + address.getDistrict() + "','" + address.getCity()
				+ "','" + address.getProvince() + "','" + address.getCountry() + "'," + address.isAssurance()
				+ ",'" + address.getDescription() + "'," + address.getTotalPrice() + "," + address.getDeliveryCost()
				+ ")";
		return template.update(sql);
	}
}
