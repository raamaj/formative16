<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sending Address</title>
</head>
<body>
<form action="save" method="post">
    <table>
        <h2>Sending Address</h2>
        <tr>
            <td>Recipient</td>
            <td>:</td>
            <td><input type="text" name="recipient" id="recipient"></td>
        </tr>
        <tr>
            <td>Gender</td>
            <td>:</td>
            <td>
                <input type="radio" name="gender" id="male" value="Male">Male
                <input type="radio" name="gender" id="female" value="Female">Female
            </td>
        </tr>
        <tr>
            <td>Street</td>
            <td>:</td>
            <td><input type="text" name="street" id="street"></td>
        </tr>
        <tr>
            <td>Zip Code</td>
            <td>:</td>
            <td><input type="text" name="zipCode" id="zipCode"></td>
        </tr>
        <tr>
            <td>District</td>
            <td>:</td>
            <td><input type="text" name="district" id="district"></td>
        </tr>
        <tr>
            <td>City</td>
            <td>:</td>
            <td><input type="text" name="city" id="city"></td>
        </tr>
        <tr>
            <td>Province</td>
            <td>:</td>
            <td><input type="text" name="province" id="province"></td>
        </tr>
        <tr>
            <td>Country</td>
            <td>:</td>
            <td>
                <select name="country" id="country">
                    <option value="Indonesia">Indonesia</option>
                    <option value="Singapore">Singapore</option>
                    <option value="Malaysia">Malaysia</option>
                    <option value="Australia">Australia</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Assurance? (10%)</td>
            <td>:</td>
            <td><input type="checkbox" name="assurance" id="assurance" value="true"></td>
        </tr>
        <tr>
            <td>Description</td>
            <td>:</td>
            <td>
                <textarea name="description" id="description" cols="30" rows="10"></textarea>
            </td>
        </tr>
        <tr>
            <td>Total Price</td>
            <td>:</td>
            <td><input type="number" name="totalPrice" id="totalPrice"></td>
        </tr>
        <tr>
            <td>Delivery service cost</td>
            <td>:</td>
            <td><input type="number" name="deliveryCost" id="deliveryCost"></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td><input type="submit" value="Send"></td>
        </tr>
    </table>
</form>
<script type="text/javascript">
function chgFlag(chk){
    var box = document.getElementById("assurance");
    box.value = chk;
  }
</script>
</body>
</html>