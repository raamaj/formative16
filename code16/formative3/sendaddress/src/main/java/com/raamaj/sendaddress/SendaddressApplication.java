package com.raamaj.sendaddress;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendaddressApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendaddressApplication.class, args);
	}

}
