package com.raamaj.sendaddress.controller;

import java.util.List;

import com.raamaj.sendaddress.model.Address;
import com.raamaj.sendaddress.service.AddressService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
public class AddressController {
    
    private AddressService service;

    public AddressController(AddressService service) {
        this.service = service;
    }

    @GetMapping("/address")
    public List<Address> getAll() {
        return service.getAllAddress();
    }

    @GetMapping("/address/{id}")
    public Address getById(@PathVariable(value = "id") int id) {
        return service.findAddressById(id);
    }

    @GetMapping("/address/{id}/delete")
    public ResponseEntity<Object> deleteAddress(@PathVariable(value = "id") int id) {
        service.deleteAddress(id);

        return ResponseEntity.ok().build();
    }

    @PostMapping(value="/address/{id}/update")
    public ResponseEntity<Object> postMethodName(@RequestBody Address address, @PathVariable int id) {
        return service.updateAddress(address, id);
    }
    
}
