package com.raamaj.sendaddress.service;

import java.util.List;
import java.util.Optional;

import com.raamaj.sendaddress.model.Address;
import com.raamaj.sendaddress.repository.AddressRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    
    @Autowired
    AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> getAllAddress() {
        return addressRepository.findAll();
    }

    public Address findAddressById(int id) {
        return addressRepository.findById(id);
    }

    public void deleteAddress(int id) {
        addressRepository.deleteById(id);
    }

    public ResponseEntity<Object> updateAddress(Address address, int id) {
        Optional<Address> getAddress = Optional.ofNullable(addressRepository.findById(id));

        if (!getAddress.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        address.setId(id);
        addressRepository.save(address);

        return ResponseEntity.ok().build();
    }
}
