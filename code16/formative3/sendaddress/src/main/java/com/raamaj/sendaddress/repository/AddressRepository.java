package com.raamaj.sendaddress.repository;

import java.util.List;

import com.raamaj.sendaddress.model.Address;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {
    List<Address> findAll();
    Address findById(int id);
    void deleteById(int id);
}
